# Readme
### To setup full environment to expose services via Docker 
* Let's do some docker commands before initialize container for first time. 
    * `$ cd /path/to/repository/docker-config`
        * Edit volume line in _docker-compose.yml_: `/path/to/repositories:/home/robin/repositories` to configure local path to repositories
    * `$ docker-compose up `
    * `$ docker-compose run --service-ports web`
* After container start, let's go to docker-config repository to execute the start-services.sh shell script under scripts directory.
    * `cd /home/robin/repositories/docker-config` 
    * `chmod +x scripts/*.sh`
    * `cd scripts`
    * `./start-services.sh`
* Now that systems services are available, just by the first time that we run this instructions, let's execute this lines below:
    *  `$ mysql -u root -p` 
    * Write the paswod provided by docker maintainer
        *  `GRANT ALL PRIVILEGES ON *.* TO 'robin'@'localhost' IDENTIFIED BY 'vinitela';`
        *  `GRANT ALL PRIVILEGES ON *.* TO 'robin'@'%' IDENTIFIED BY 'vinitela';`
        *  `FLUSH PRIVILEGES;`
        *  `CREATE DATABASE robin_users`

After ran above instructions, all environment is ready to start all services. 
each service needs be execute indivually. It's posible that services block the terminal, in that case we will open a new terminal to connect to run container to start another service.

##### Property of robin.mx